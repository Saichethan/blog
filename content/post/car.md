---
title: "Don't buy a new car!"
date: 2019-01-06T23:01:54+05:30
draft: false
---

Buying a car is a dream for many people, but is that worth your money and effort. In this post I will draw some insights.

Even if we use car everyday to get to work it's still cheaper to use a share ride service and we can choose the level of luxary we want.

It is found that new cars drops in value by 30% in first year and upto 50% in three years of purchase and most people borrow money to buy a car which means we are not only loosing money to buy a car(depreciation) but also paying interest for that money. 

Some people get loan for car to reduce the taxes and low bank interest rates even considering this two factors we end up loosing more money, it's just like buying a whole lot of cookies because there is a 10% off on the price.

**Example :**


Let's say you get a loan of 30 Lakhs with interest rate(p.a) 9.5% 

if we choose to pay within 1 year of purchase then total amount payable is Rs. 31,56,606. In one year value of the car depreciate so, value of car after one year is 21 Lakhs.

if we choose to pay within 3 years of purchase then total amout payable is Rs. 34,59,559. In three years value of the car depreciates so, value of car after three years is nearly 15 Lakhs

These are just basic prices there will be insurance bills, road taxes, tickets, car maintenance and many more.

## Some insights
Ride sharing is better because

* we don't need to park while going to malls
* can choose level of comfort
* optimum use of vehicles rather than idling in garages and parking lots.
* no need to check for bills, insurance, tickets.

## Alternatives
* Car Pooling
* Public Transport
* Bike
* Used Cars

>> Its better not to buy a new cars and if possible don't buy a car but, if ride sharing is not an option for you atleat try to buy used cars. Invest remaining amount for future.
 

 