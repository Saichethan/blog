---
title: "Dense breast imaging"
date: 2019-04-03T23:01:54+05:30
draft: false
---

Breast cancer is one of the most common cancer in women worldwide. Currently, the average risk of a woman in the United States developing breast cancer sometime in her life is about 12%. This means there is a 1 in 8 chance she will develop breast cancer. This also means there is a 7 in 8 chance she will never have the disease. In this post I propose a solution to detect malignant tissues in dense breast woman.


## Introduction
Breast cancer starts when cells in the breast begin to grow out of control. These cells usually form a tumor that can often be seen on an x-ray or felt as a lump. The tumor is malignant (cancer) if the cells can grow into (invade) surrounding tissues or spread (metastasize) to distant areas of the body. Breast cancer occurs almost entirely in women, but men can get breast cancer, too.

Breast cancers can start from different parts of the breast. Most breast cancers begin in the ducts that carry milk to the nipple (ductal cancers). Some start in the glands that make breast milk (lobular cancers). There are also other types of breast cancer that are less common.

There are many ways of approaching breast imaging, and because there's so many technologies people get confused. Diagnosing the type and stage of breast cancer may require several techniques and different kinds of tests. The standard and most common screening method is the X-ray imaging of the breast, or mammography

The below is a closeup image of a dense breast mammogram 

{{< amp-img width="240" height="305" layout="responsive" src="https://saichethan.github.io/website/images/dense.jpg" alt="Dense Breast Mammogram" >}}


In mammography doctors identify little tiny white dots on mammogram, which represent calcifications, the earliest stage of breast cancer. But it is difficult to find the difference between calcification and fat content in woman with very dense breast tissues.

## Possible solution

**3D-mammograph or 360 view** : Mammogram should be taken in all possible angles and then identifying the calcification. Generally when mammogram is taken in woman with dense breast tissue the fat content of breast can overlap the calcium particles, taking mammogram in different angles reduce the overlap as we can check it layerwise which can reduce false positives.


But I think the problem with this solution is, mammography is generally used for asymptomatic women, because mammograms are x-ray tests, they expose the breasts to radiation. The amount of radiation from each mammogram is low, but it can still add up over time. One more concern is if the fat content and calcium particles are overlapping in same layer then we might not get an accurate result.