---
title: "Gender pay gap"
date: 2019-03-02T21:54:34+05:30
draft: false
---

Feminism by defination is the belief men and women should have equal rights it is the theory of political, economical, and social equalities of both sexes. The word feminism is often consider as taboo, many woman are choosing not to be identified as feminist because people might think them as male haters. It's because it has femine in word. Financial Independane of woman is one of the important way to achieve feminism ideals. In this post I draw some insights about gender pay gaps.

## So what is gender pay gap

 The Gender pay gap is the difference in median earnings between man and woman , often it’s expressed as percentage of men’s earning. The gender pay gap in United States is 20% as of 2016 reported by Institute for women’s policy and research i.e U.S working women earned 80 cents for every dollar men earned. Institute for women’s policy and research also found that gap is even worse when broken by race, black women earned 63 cents and hispanic women earned 54 cents for every dollar earned by their male counterparts.

## Common reason's 

 * The differences in occupations account for big part of pay gap
 * Motherhood
 * Education

These are some of the standard reason's, but are they really reasons for pay gap.


## What actually happen's
* Even when companies offer paid family leave, men are less likely to use them. 
* When working women having children, they are more likely to experience a paycut
* Contrast to that men often get a fatherhood bonus
* And even the women who choose not have children are experiencing a paygap
* When you compare men and women with the same level of education, the pay gap is even large

There is not even a single country in this world where this is not happening. Even many working woman in developing countries doesn't have financial independance over the money they earn. 

## What happens when paid equally
* Studies found that equal pay would cut the poverty rate of working women in half.
* It also say’s it will produce an additional income of around $512 billion to U.S economy.
* Another report found that if women in developing countries are paid as much as men, they could add $2 trillion which will in turn increase the pay.
* If as many women worked as men, the IMF estimates GDP would increase by 27% in India.

So,  equal pay is not only morally important but also finacially to the country's economy.

>> Am I feminist? I don't know yet, all I know and believe in is that "equal rights for all humans" (i.e equal pay, respect, freedom etc..,) if this is what feminism is then I am a feminist. 